import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css'

// Components Import statments
import Game from './components/game/Game';


export default class App extends Component {


    render() {
        return (
                <div className="container">
                    <Game></Game>
                </div>
        );
    }
}
