import React, { Component } from 'react'
import Board from './Board';
import User from './User';
import '../../index.css'

export default class Game extends Component {
	state = {
		players: [],
		exit: false
	}

	getPlayers(data) {
		const newState = this.state.players.concat(data)
		this.setState({players: newState})
	}
	exitGame(){
		this.setState({
			exit: true
		})
	}
	render() {
		return(
			<div className="game">
				<h1 className="text-center mt-2">Tic Tac Toe</h1>

				{ 
					(this.state.players.length <= 0 && this.state.exit == false) &&
						<User triggerParentUpdate={this.getPlayers.bind(this)}></User>
				}
				{
					(this.state.players.length > 0 && this.state.exit == false) &&
						<Board userData={this.state.players} triggerExitGame={this.exitGame.bind(this)}></Board>
				}
				{
					(this.state.exit == true) &&
					<div className="text-center mt-4 mb-4">
						Thank you for playing TicTacToe
					</div>
				}
			</div>
		)}
}