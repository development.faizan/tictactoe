import React, { Component } from 'react'
import { tsImportEqualsDeclaration, arrayExpression } from '@babel/types';
import { Modal, Button } from 'react-bootstrap';

export default class Board extends Component {

	state = {
		players: this.props.userData,
		totalTurns: 0,
		checks: Array(9).fill(""),
		winStates: [7, 56, 448, 73, 146, 292, 273, 84],
		gameOver: false,
		modelShow: true,
		winner: null
	}
	
	constructor(props){
		super(props)
		this.playerTurn = 0
		this.getFirstPlayer()
		
	}

	createBoard(){
		let board = []
		for (let i = 0; i < 9; i++) {
			board.push(<div id={"cell-" + i} className="box" key={Math.random()} >{
				this.state.checks[i]
			}</div>)
		}
		return board
	}

	getFirstPlayer(){
		if(!this.state.gameOver){
			this.playerTurn = Math.floor(20*Math.random())%2
		}
	}

	nextRound(){
		let players = [...this.state.players]
		players.forEach(player => {
			player.score = 0
		})
		this.getFirstPlayer()
		this.setState({
			checks: Array(9).fill(""),
			totalTurns: 0,
			gameOver: false,
			modelShow: true,
			winner: null
			
		})
	}
	checkGameState(){
		for(let i=0; i < this.state.winStates.length; i++){
			let win = this.state.winStates[i]
			let players = [...this.state.players]
			if (((win & this.state.players[this.playerTurn].score) === win)) {
				this.setState({
					gameOver: true,
					winner: players[this.playerTurn].name
				})
				break
			}else if(this.state.totalTurns == 8){
				this.setState({
					gameOver: true,
					winner: "draw"
				})
				break
			}
		}
	}
	handleClick(e){
		let boxIndex = e.target.id[5]
		let players = [...this.state.players]
		let newChecks = [...this.state.checks]
		
		if(!this.state.gameOver && e.target.innerHTML != "X" && e.target.innerHTML != "O"){
			newChecks[boxIndex] = this.playerTurn == 0 ? "X" : "O"
			players[this.playerTurn].score = players[this.playerTurn].score + Math.pow(2, boxIndex)
			
			this.setState({
				checks: newChecks,
				totalTurns: this.state.totalTurns + 1
			})
			this.checkGameState()
			this.playerTurn = 1 - this.playerTurn;
		}
	}
	handleYes(){
		this.nextRound()
		this.setState({
			modelShow: true
		})
	}
	handleNo(){
		this.setState({
			modelShow: false
		})
	}

	render() {
		return (
			<div>
				{
					(this.state.gameOver && this.state.modelShow) &&
					<Modal show={this.state.modelShow} onHide={this.handleNo}>
						<Modal.Header closeButton>
							<Modal.Title>Play Again</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<p>
								{
									this.state.winner == "draw" ? "Its a draw" : ("The winner is " + (this.playerTurn != 0 ? "(X) " : "(O) ") + this.state.winner)
								}
							</p>
							<p>Would you like to play another game?</p>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="primary yesButtonColor" onClick={this.handleYes.bind(this)}>Yes</Button>
							<Button variant="secondary noButtonColor" onClick={this.handleNo.bind(this)}>No</Button>
						</Modal.Footer>
					</Modal>
				}
				<div className="d-flex justify-content-center mt-3">
					<div className="infoHeader " >
						{
							(this.state.winner == null) &&
							<div>
								
								Player { (this.playerTurn == 0 ? "(X) " : "(O) ") + ": " + this.state.players[this.playerTurn].name }
							</div>
						}
						{
							(this.state.winner != null && this.state.winner != "draw") &&
							<div>Winner
								{ " " + (this.playerTurn != 0 ? "(X) " : "(O) ") + ": " + this.state.winner }
							</div>
						}
						{
							(this.state.winner == "draw") &&
							<div>
								Its a Draw
							</div>
						}
					</div>
				</div>
				<div className="d-flex justify-content-center mt-2 mb-3">
					{
						this.state.players &&
						<div id="board" onClick={this.handleClick.bind(this)}>
							{
								this.createBoard()
							}
						</div>
					}
				</div>
				<div className="d-flex justify-content-between mr-5 ml-5 mb-3">
						<button type="button" class="btn btn-success yesButtonColor" onClick={this.nextRound.bind(this)}>New Game</button>
						<button type="button" class="btn btn-danger" onClick={this.props.triggerExitGame}>Exit</button>
				</div>
			</div>
		)
	}
}
