import React, { Component } from 'react'

export default class User extends Component {

    state = {
        players: [
            {
                name: "",
                score: 0,
            },
            {
                name: "",
                score: 0,
            }
        ]
    }

    handlePlayer1Name(e) {
        let players = [...this.state.players]
        players[0].name = e.target.value
        this.setState({players})
    }
    handlePlayer2Name(e) {
        let players = [...this.state.players]
        players[1].name = e.target.value
        this.setState({players})
    }
    submitHandler(e) {
        e.preventDefault()
        if(this.state.players[0].name != this.state.players[1].name)
            this.props.triggerParentUpdate(this.state.players)
        else
            alert("Player 1 and Player 2 names cannot be the same")
    }

    render() {
        return (
            <form className="PlayerData" onSubmit={this.submitHandler.bind(this)}>
                <div className="row">
                    <div className="from-group col-12">
                        <label htmlFor="player1">Player (X) Name</label>
                        <input type="text" className="form-control" id="player1" placeholder="Enter your name" value={this.state.players[0].name} onChange={this.handlePlayer1Name.bind(this)} ></input>
                    </div>
                </div>
                <div className="row">
                    <div className="from-group col-12">
                        <label htmlFor="player2">Player (O) Name</label>
                        <input type="text" className="form-control" id="player2" placeholder="Enter your name" value={this.state.players[1].name} onChange={this.handlePlayer2Name.bind(this)} ></input>
                    </div>
                </div>

                <div className="row mt-4">
                    <div className="d-flex justify-content-center col-md-12">
                        <button type="submit" className="btn btn-primary yesButtonColor">Start</button>
                    </div>
                </div>
            </form>
        )
    }
}
